#!/usr/bin/bash

set -e
sudo yum install -y git ansible
echo "Cloning ansible playbook"
git clone https://gitlab.com/ITOpsTube-Webinars/golden-image.git 
echo "Running build"
ansible-playbook golden-image/golden-image-ami-2/main-site.yml

